﻿using System;

namespace My_Awesome_Program
{
    public static class Game
    {
        static string myName;
        static int Scenarios = 3;
        static string[] StoryContentOne = {
            "- Hello, what's your name?",
            "- Hi " + myName + "!, my name is ALPHA.\n- I have been scripted by an autonomous organism from \nplanet earth. He has many plans for you and me."
        };
        static string[] StoryContentTwo = {
            "- Tell me more about yourself. Are you a boy or a girl?",
            "- So you're a boy! UwU I'm so nervous...\nThis is my first interaction with a human.",
            "- So you're a girl! This is my first human interaction.",
            "- I understand you probably don't remember anything but\njust know that I'm here to help.\n- You have been chosen out of 5 subjects for an \nexperiment. The aim is for you to figure out how to \nescape."
        };
        static string[] storyContentThree;
 
        public static void StartGame()
        {
            Console.WriteLine("////////////////////////////////////////////////////////");
            Console.WriteLine("//////////////////////// ALPHA /////////////////////////");
            Console.WriteLine("///////////////////// VERSION 1.0 //////////////////////");
            Console.WriteLine("////////////////////////////////////////////////////////\n");

            NameCharacter();

            Console.WriteLine();
            System.Threading.Thread.Sleep(600);

            Choice();
            EndGame();
        }

        static void NameCharacter()
        {
            TextDelay.DisplayTime("- Hello, what's your name?", ConsoleColor.Green);
            myName = Console.ReadLine();

            TextDelay.DisplayTime("- Hi " + myName + "!, my name is ALPHA.\n- I have been scripted by an autonomous organism from \nplanet earth. He has many plans for you and me.", ConsoleColor.Green);
        }

        static void Choice()
        {
            string input = "";
            string gender = "";

            bool answerCorrect = false;
            bool showInitialMessage = true;

            while (!answerCorrect)
            {
                if (showInitialMessage)
                {
                    TextDelay.DisplayTime(StoryContentTwo[0], ConsoleColor.Green);
                }

                input = Console.ReadLine();
                input = input.ToUpper();

                if (input == "BOY")
                {
                    TextDelay.DisplayTime(StoryContentTwo[1], ConsoleColor.Green);
                    gender = "Boy";
                    answerCorrect = true;
                }
                else if (input == "GIRL")
                {
                    TextDelay.DisplayTime(StoryContentTwo[2], ConsoleColor.Green);
                    gender = "Girl";
                    answerCorrect = true;
                }
                else
                {
                    TextDelay.DisplayTime("- Your answer \"" + input + "\" was not recognised. Please try again.", ConsoleColor.Green);
                    showInitialMessage = false;
                }

                TextDelay.DisplayTime(StoryContentTwo[3], ConsoleColor.Green);

            }
        }

        static void EndGame()
        {
            Console.ReadKey();
        }


    }

    class Item
    {

    }

    class Program
    {
        static void Main()
        {
            Console.Title = "Alpha";
            Console.ForegroundColor = ConsoleColor.Green;
            // Console.WindowHeight = 40;

            Game.StartGame();
        }
    }
}
