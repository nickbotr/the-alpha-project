using System;

namespace My_Awesome_Program
{
    public class TextDelay
    {
        public static void DisplayTime(string message, ConsoleColor color)
        {
            for (int i = 0; i < message.Length; i++)
            {
                if(i < message.Length-1)
                {
                    Console.ForegroundColor = color;
                    Console.Write(message[i]);
                    System.Threading.Thread.Sleep(60);
                } else if(i == message.Length-1)
                {
                    Console.WriteLine(message[i]);
                    System.Threading.Thread.Sleep(60);
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
            }
        }
    }
}
